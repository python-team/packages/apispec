Source: apispec
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jelmer Vernooĳ <jelmer@debian.org>
Build-Depends: debhelper-compat (= 13), python3-setuptools, python3-all, python3-sphinx, python3-sphinx-rtd-theme, python3-sphinx-issues, python3-pytest, python3-marshmallow, python3-yaml, flit, pybuild-plugin-pyproject
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/marshmallow-code/apispec
Vcs-Browser: https://salsa.debian.org/python-team/packages/apispec
Vcs-Git: https://salsa.debian.org/python-team/packages/apispec.git

Package: python3-apispec
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-apispec-doc
Description: pluggable API specification generator (Python 3)
 This package contains a pluggable API specification generator. It currently
 supports the OpenAPI Specification (f.k.a. the Swagger specification).
 The features are:
    - Supports the OpenAPI Specification (versions 2 and 3)
    - Framework-agnostic
    - Built-in support for marshmallow
    - Utilities for parsing docstrings
 .
 This package installs the library for Python 3.

Package: python-apispec-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Recommends: python3-yaml
Multi-Arch: foreign
Description: pluggable API specification generator (common documentation)
 This package contains a pluggable API specification generator. It currently
 supports the OpenAPI Specification (f.k.a. the Swagger specification).
 The features are:
    - Supports the OpenAPI Specification (versions 2 and 3)
    - Framework-agnostic
    - Built-in support for marshmallow
    - Utilities for parsing docstrings
 .
 This is the common documentation package.
