apispec (6.8.1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version.
  * d/tests/control: Fix typo, use Restrictions instead of Restriction.

 -- Emmanuel Arias <eamanu@debian.org>  Sat, 15 Feb 2025 19:35:14 -0300

apispec (6.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * d/tests/control: Add Restriction: allow-stderr. Also run wrap-and-sort.

 -- Emmanuel Arias <eamanu@debian.org>  Thu, 28 Nov 2024 19:36:20 -0300

apispec (6.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Emmanuel Arias <eamanu@debian.org>  Sun, 05 May 2024 20:52:26 -0300

apispec (6.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * d/control: Add pybuild-plugin-pyproject and flit as B-Depends.
    - Remove dh-sequence-python3.
  * Set upstream metadata fields: Security-Contact (routine-update)
  * d/patches/remove_openapi_spec_validator_reference.patch: Remove
    references to openapi_spec_validator from tests/utils.py because
    this module is not in Debian yet.
  * d/rules: Remove PYBUILD_TEST_ARGS variable, it is not longer needed.
  * d/patches/edit_test_to_fix_PytestReturnNotNoneWarning.patch: Remove
    patch, new upstream release change the tests.
  * d/patches/change-conf-file.patch: Add patch to pin the
    version in docs/conf.py. It used the importlib.metadata.version() method
    to get the documentation version.
  * d/python-apispec-doc.doc-base: Add doc-base registration.

 -- Emmanuel Arias <eamanu@debian.org>  Fri, 12 Apr 2024 16:05:10 -0300

apispec (6.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * d/control:
    - Set Rules-Requires-Root to no
    - Bump standards version to 4.6.2 from 4.6.0
    - Remove Testsuite: autopkgtest-pkg-python, no longer needed
  * d/copyright:
    - Update copyright year to 2023 for debian/* files
    - Add myself to copyright for debian/* files
  * d/tests/upstream:
    - Correct py3versions from -r option to -s
    - Remove redirection to /dev/null
  * d/upstream/metadata:
    - Add Archive: Github
  * d/patches/edit_test_to_fix_PytestReturnNotNoneWarning:
    - Fix PytestReturnNotNoneWarning in build test by changing function name
      from test_plugin_factory to plugin_factory

 -- Ileana Dumitrescu <ileanadumitrescu95@gmail.com>  Mon, 24 Jul 2023 17:06:35 +0300

apispec (5.2.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-apispec-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 15:26:30 +0000

apispec (5.2.2-2) unstable; urgency=medium

  * Team upload.
  * [2cd97c1] run upstream tests as autopkgtest

 -- Joseph Nahmias <jello@debian.org>  Wed, 13 Jul 2022 22:52:35 -0400

apispec (5.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 02 Jun 2022 15:40:17 -0400

apispec (5.2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/control: Bump debhelper compat to v13.

 -- Boyuan Yang <byang@debian.org>  Thu, 05 May 2022 13:18:03 -0400

apispec (5.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 5.1.1
    + Also make the build reproducible. (Closes: #988978)
  * debian/gbp.conf: Fix format; use debian format instead of kali.
  * Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Fri, 15 Oct 2021 16:45:33 -0400

apispec (3.3.1-1) unstable; urgency=medium

  * Initial upload to Debian. Closes: #986751

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 11 Apr 2021 08:10:48 +0100

apispec (3.3.1-0kali2) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Configure git-buildpackage for Kali
  * Add GitLab's CI configuration file
  * debian/rules: fix for pytest version 6

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 05 Feb 2021 12:02:49 +0100

apispec (3.3.1-0kali1) kali-dev; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 08 Jun 2020 16:06:43 +0200
